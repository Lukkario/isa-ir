import sys
import time

# from picamera.array import PiRGBArray
# from picamera import PiCamera
import numpy as np
import cv2
import matplotlib.pyplot as plt

# from imutils.video import VideoStream

# cam = cv2.VideoCapture(
#    'nvarguscamerasrc ! video/x-raw(memory:NVMM), width=3820, height=2464, format=(string)NV12, framerate=21/1 ! nvvidconv flip-method=0 ! video/x-raw, width=960, height=616, format=BGRx ! videoconvert ! video/x-raw, format=BGR ! appsink',
#    cv2.CAP_GSTREAMER)

time.sleep(2.0)

# lower_green = (0, 100, 50)
# upper_green = (100, 255, 255)


color_tolerance = 3

lower_green1 = (0, 100, 50)
upper_green1 = (100, 255, 255)

lower_green2 = (0 + color_tolerance, 100, 50)
upper_green2 = (100 + color_tolerance, 255, 255)

# paused = False
# roiSize = (16, 16)  # roi size on the scaled down image (converted to HSV)

# # initialize serial communication
# ser = serial.Serial(port='/dev/ttyACM0', baudrate=57600, timeout=0.05)

# frame = cv2.imread("img/1.jpg")
hist = []


def processFrames():
    while True:
        frame = cv2.imread("img/1.jpg")
        print(frame.shape)
        frame_HSV = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        mask = cv2.inRange(frame_HSV, lower_green1, upper_green1)
        mask_blur = cv2.medianBlur(mask, 5)
        canny_edge = cv2.Canny(mask_blur, 0, 0)

        circles = cv2.HoughCircles(mask_blur, cv2.HOUGH_GRADIENT, 1, frame.shape[0]/2, param1=20, param2=20, minRadius=0, maxRadius=0)

        if circles is not None:
            circles = np.uint16(np.around(circles))
            for i in circles[0, :]:
                # draw the outer circle
                cv2.circle(frame, (i[0], i[1]), i[2], (255, 0, 0), 2)
                # draw the center of the circle
                cv2.circle(frame, (i[0], i[1]), 2, (0, 0, 255), 3)

        cv2.imshow('frame', frame)
        cv2.imshow('mask_blur', mask_blur)
        cv2.imshow('canny_edge', canny_edge)
        if cv2.waitKey(0):
            break
        # frame = cv2.imread("img/1.jpg")
        # print(frame.shape)
        # frame_HSV = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        # mask = cv2.inRange(frame_HSV, lower_green1, upper_green1)
        # img_blur = cv2.medianBlur(mask, 5)
        # circles = cv2.HoughCircles(mask, cv2.HOUGH_GRADIENT, 1, 2, param1=50, param2=30, minRadius=0, maxRadius=frame.shape[0])
        # # print(circles)
        # if circles is not None:
        #     circles = np.uint16(np.around(circles))
        #     for i in circles[0, :]:
        #         # Draw outer circle
        #         cv2.circle(frame, (i[0], i[1]), i[2], (0, 255, 0), 2)
        #         # Draw inner circle
        #         cv2.circle(frame, (i[0], i[1]), 2, (0, 0, 255), 3)
        #
        # cv2.imshow("CAMERA", frame)
        # cv2.imshow("MASK1", mask)
        # # cv2.imshow("OBJECTS", obj)
        # cv2.imshow("CONTOURS", img_blur)
        # if cv2.waitKey(1) & 0xFF == ord('q'):
        #     break


if __name__ == '__main__':
    processFrames()
    # cv2.destroyAllWindows()
    # cam.stop()
    sys.exit(0)
